package br.com.viniciusfragellisenff.testjuno

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import br.com.viniciusfragellisenff.testjuno.features.listabusca.presenter.ListaEBuscaRepositoriosPresenter
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import br.com.viniciusfragellisenff.testjuno.model.ResponseVO
import br.com.viniciusfragellisenff.testjuno.model.database.DataBaseHelper
import br.com.viniciusfragellisenff.testjuno.model.services.TestJunoProvider
import br.com.viniciusfragellisenff.testjuno.model.services.TestJunoService
import io.reactivex.disposables.CompositeDisposable

import org.junit.Test
import org.junit.runner.RunWith

import org.junit.Assert.*
import org.mockito.Mockito.mock

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class ServicesTestesAndRXTests {

    @Test
    fun TesteSucessBuscaRepositorio() {
        var endTest = false;
        val appContext = InstrumentationRegistry.getTargetContext()
        var viewCallback: ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback = object : ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback {
            override fun onBuscaRepositorioError() {
                assertEquals(false, true)
                endTest = true
            }

            override fun onSucessoBuscaRepositorio(lista: List<RepositorioEntity>) {
                assertEquals(2034023, lista[0].id)
                endTest = true
            }
        }
        var presenter: ListaEBuscaRepositoriosPresenter = ListaEBuscaRepositoriosPresenter(appContext, viewCallback)
        presenter.taskBuscaRepositorio("teste")
        while(!endTest) Thread.sleep(300)
    }

    @Test
    fun TesteChamadaViewSucessBuscaRepositorio() {
        var endTest = false;
        val appContext = InstrumentationRegistry.getTargetContext()
        var viewCallback: ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback = object : ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback {
            override fun onBuscaRepositorioError() {
                assertEquals(false, true)
                endTest = true
            }

            override fun onSucessoBuscaRepositorio(lista: List<RepositorioEntity>) {
                assertEquals(true, true)
                endTest = true
            }
        }
        var presenter: ListaEBuscaRepositoriosPresenter = ListaEBuscaRepositoriosPresenter(appContext, viewCallback)
        presenter.taskBuscaRepositorio("teste")
        while(!endTest) Thread.sleep(300)
    }

    @Test
    fun TesteDeBounceRXInSearchRepositorios() {
        var count = 0;
        val appContext = InstrumentationRegistry.getTargetContext()
        var viewCallback: ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback = object : ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback {
            override fun onBuscaRepositorioError() {
                assertEquals(false, true)
                count++
            }

            override fun onSucessoBuscaRepositorio(lista: List<RepositorioEntity>) {
                assertEquals(true, true)
                count++
            }
        }
        var presenter: ListaEBuscaRepositoriosPresenter = ListaEBuscaRepositoriosPresenter(appContext, viewCallback)
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        presenter.taskBuscaRepositorio("teste")
        while(true){ Thread.sleep(10000); break }
        assertEquals(1,count)
    }

    @Test
    fun TesteLockTheFilterPullRequest() {
        var count = 0
        val appContext = InstrumentationRegistry.getTargetContext()
        var viewCallback: ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback = object : ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback {
            override fun onBuscaRepositorioError() {
                count++
            }

            override fun onSucessoBuscaRepositorio(lista: List<RepositorioEntity>) {
                count++
            }
        }
        var presenter: ListaEBuscaRepositoriosPresenter = ListaEBuscaRepositoriosPresenter(appContext, viewCallback)
        presenter.taskBuscaRepositorio("")
        while(true){ Thread.sleep(10000); break }
        assertEquals(1,count)
    }
}
