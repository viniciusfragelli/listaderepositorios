package br.com.viniciusfragellisenff.testjuno

import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import br.com.viniciusfragellisenff.testjuno.model.database.DAORepositorios
import br.com.viniciusfragellisenff.testjuno.model.database.DataBaseHelper
import br.com.viniciusfragellisenff.testjuno.model.database.ProviderRepositorios
import junit.framework.Assert.fail
import org.junit.Assert
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class ServiceDataBaseTests {

    @Test
    fun InitDataBase() {
        val appContext = InstrumentationRegistry.getTargetContext()
        var dataBaseHelper = DataBaseHelper(appContext);
        Assert.assertEquals(true, dataBaseHelper.isOpen)
    }

    @Test
    fun verificaSeCriaTabela() {
        val appContext = InstrumentationRegistry.getTargetContext()
        var dataBaseHelper = DataBaseHelper(appContext)
        dataBaseHelper.writableDatabase
    }

    @Test
    fun verificaCriaçãoDAO() : DAORepositorios {
        val appContext = InstrumentationRegistry.getTargetContext()
        var ex: Exception? = null
        var daoRepositorios: DAORepositorios? = null
        try {
            var dataBaseHelper = DataBaseHelper(appContext)
            dataBaseHelper.writableDatabase
            dataBaseHelper.readableDatabase
            daoRepositorios = DAORepositorios(ProviderRepositorios(dataBaseHelper))
        }catch (e: Exception){
            ex = e
        }
        Assert.assertEquals(null,ex)
        if(daoRepositorios == null)fail("Erro ao inicializar vaiavel")
        return daoRepositorios!!
    }


    @Test
    fun verificaSeGravaDados() : DAORepositorios {
        val daoRepositorios: DAORepositorios = verificaCriaçãoDAO()
        var teste = RepositorioEntity()
        teste.id = 1
        teste.name = "teste"
        Assert.assertNotEquals(null,daoRepositorios.save(teste))
        return daoRepositorios
    }

    @Test
    fun verificaSeGravaDadosELeDados() {
        val daoRepositorios: DAORepositorios = verificaCriaçãoDAO()
        val teste = daoRepositorios.queryByID(1)
        Assert.assertNotEquals(0,teste?.size)
        if(teste != null && teste.size > 0){
            Assert.assertEquals(1, teste?.get(0)?.id)
            Assert.assertEquals("teste", teste?.get(0)?.name)
        }
    }
}