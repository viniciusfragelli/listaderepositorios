package br.com.viniciusfragellisenff.testjuno.model.database

import android.util.Log
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.dao.DaoManager
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.support.ConnectionSource
import java.sql.SQLException
import javax.inject.Inject
import javax.inject.Provider

class ProviderRepositorios(private var dbHelper: DataBaseHelper) : Provider<RuntimeExceptionDao<RepositorioEntity, String>> {

    private var connectionSource: ConnectionSource? = null

    init {
        processInject()
    }

    @Inject
    fun processInject() {
        connectionSource = dbHelper.connectionSource
    }

    override fun get(): RuntimeExceptionDao<RepositorioEntity, String>? {
        try {
            val dao = DaoManager.createDao<Dao<RepositorioEntity, String>, RepositorioEntity>(connectionSource, RepositorioEntity::class.java)
            return RuntimeExceptionDao<RepositorioEntity, String>(dao)
        } catch (e: SQLException) {
            Log.e("SQL", e.toString())
        } catch (ex: Exception) {
            Log.e("EX", ex.toString())
        }

        return null
    }
}