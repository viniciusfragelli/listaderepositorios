package br.com.viniciusfragellisenff.testjuno.bases

import io.reactivex.disposables.CompositeDisposable

open class BaseInteractorService(val compositeDisposable: CompositeDisposable) {

}