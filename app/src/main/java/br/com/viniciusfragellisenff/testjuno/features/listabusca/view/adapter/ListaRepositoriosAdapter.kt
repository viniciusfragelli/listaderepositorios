package br.com.viniciusfragellisenff.testjuno.features.listabusca.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.viniciusfragellisenff.testjuno.R
import br.com.viniciusfragellisenff.testjuno.extensions.toFormatBrazil
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import kotlinx.android.synthetic.main.repositorio_adapter.view.*
import org.jetbrains.anko.sdk25.coroutines.onClick

class ListaRepositoriosAdapter(private val context: Context, private var lista: List<RepositorioEntity>, private val onClickItem: ((RepositorioEntity) -> Unit)) : RecyclerView.Adapter<RepositorioViewHolder>() {

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): RepositorioViewHolder = RepositorioViewHolder(LayoutInflater.from(context).inflate(R.layout.repositorio_adapter, viewGroup, false))

    override fun getItemCount(): Int = lista.size

    override fun onBindViewHolder(view: RepositorioViewHolder, position: Int) {
        val repo = lista[position]
        with(view.itemView){
            tvIDRepositorio.text = repo.id.toString();
            tvNomeRepositorio.text = repo.name ?: "Sem registro"
            tvPrivate.text = if(repo.private) "Sim" else "Não"
            tvUltimaAtualizacao.text = repo.created_at.toFormatBrazil() ?: "Sem registros"
            llRepositorio.onClick { onClickItem(repo) }
        }
    }


}

class RepositorioViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}
