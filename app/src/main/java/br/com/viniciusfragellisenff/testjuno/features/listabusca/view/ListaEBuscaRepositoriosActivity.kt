package br.com.viniciusfragellisenff.testjuno.features.listabusca.view

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import br.com.viniciusfragellisenff.testjuno.R
import br.com.viniciusfragellisenff.testjuno.bases.BaseActivity
import br.com.viniciusfragellisenff.testjuno.extensions.gone
import br.com.viniciusfragellisenff.testjuno.extensions.show
import br.com.viniciusfragellisenff.testjuno.features.inforepositorio.view.InfoRepositoryActivity
import br.com.viniciusfragellisenff.testjuno.features.listabusca.presenter.ListaEBuscaRepositoriosPresenter
import br.com.viniciusfragellisenff.testjuno.features.listabusca.view.adapter.ListaRepositoriosAdapter
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.startActivity

class ListaEBuscaRepositoriosActivity : BaseActivity(),ListaEBuscaRepositoriosPresenter.ListaEBuscaRepositoriosViewCallback {

    private val presenter: ListaEBuscaRepositoriosPresenter by lazy { ListaEBuscaRepositoriosPresenter(this,this) }

    private var menu: Menu? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        basePresenter = presenter
        rvListaRepositorios.layoutManager = LinearLayoutManager(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = getString(R.string.lista_repo)
        etBusca.addTextChangedListener(onTextChange())
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_off,menu)
        this.menu = menu
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item?.itemId){
            R.id.menu_offline -> {
                presenter.isOfflineMode = !presenter.isOfflineMode
                if(presenter.isOfflineMode){
                    menu?.getItem(0)?.setIcon(R.mipmap.ic_offline)
                    Toast.makeText(this,"Modo busca OFFLINE ativado!",Toast.LENGTH_LONG).show()
                }else{
                    menu?.getItem(0)?.setIcon(R.mipmap.ic_online)
                    Toast.makeText(this,"Modo busca ONLINE ativado!",Toast.LENGTH_LONG).show()
                }
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun onClickListView(repo: RepositorioEntity){
        startActivity<InfoRepositoryActivity>("repo" to repo)
    }

    private fun onTextChange(): TextWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {

        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (!s.toString().isNullOrEmpty()) {
                progressBar.show()
                tvSemResultadosEncontrados.gone()
                presenter.taskBuscaRepositorio(s.toString())
            } else {
                presenter.cancelarBusca()
                rvListaRepositorios.adapter = ListaRepositoriosAdapter(this@ListaEBuscaRepositoriosActivity, mutableListOf(), { this@ListaEBuscaRepositoriosActivity.onClickListView(it) })
                progressBar.gone()
                tvSemResultadosEncontrados.show()
            }
        }

    }

    override fun onBuscaRepositorioError() {
        progressBar.gone()
        alert {
            title = getString(R.string.alerta)
            message = getString(R.string.erro_busca)
            positiveButton(getString(R.string.sim),{
                presenter.isOfflineMode = true
                etBusca.text = etBusca.text
                menu?.getItem(0)?.setIcon(R.mipmap.ic_offline)
                Toast.makeText(this@ListaEBuscaRepositoriosActivity,"Modo busca OFFLINE ativado!",Toast.LENGTH_LONG).show()
            })
            negativeButton(getString(R.string.nao),{
                presenter.isOfflineMode = false
            })
        }.show()
    }

    override fun onSucessoBuscaRepositorio(lista: List<RepositorioEntity>) {
        progressBar.gone()
        if (lista.size == 0){
            tvSemResultadosEncontrados.show()
        }
        rvListaRepositorios.adapter = ListaRepositoriosAdapter(this,lista,{ onClickListView(it) })
    }
}
