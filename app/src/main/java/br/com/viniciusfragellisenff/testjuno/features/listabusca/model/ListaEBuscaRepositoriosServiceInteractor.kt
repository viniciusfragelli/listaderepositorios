package br.com.viniciusfragellisenff.testjuno.features.listabusca.model

import br.com.viniciusfragellisenff.testjuno.bases.BaseInteractorService
import br.com.viniciusfragellisenff.testjuno.model.ResponseVO
import br.com.viniciusfragellisenff.testjuno.model.services.TestJunoProvider
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ListaEBuscaRepositoriosServiceInteractor(compositeDisposable: CompositeDisposable) : BaseInteractorService(compositeDisposable) {

    private var disposable: Disposable? = null

    fun getBuscaRepositorios(busca: String, onSuccess: ((ResponseVO) -> Unit), onError: ((Throwable) -> Unit)){
        if(disposable != null)compositeDisposable.remove(disposable!!)
        disposable = TestJunoProvider.getTestJunoService().buscaRepositorio(busca)
                .debounce(2000,TimeUnit.MICROSECONDS)
                .filter({ if(busca.isNullOrEmpty()) false else true })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    onSuccess(response)
                },{ ex ->
                    onError(ex)
                })
        compositeDisposable.add(disposable ?: return)
    }

    fun cancelarBusca(){
        compositeDisposable.remove(disposable ?: return)
    }

}