package br.com.viniciusfragellisenff.testjuno.bases

import android.support.v7.app.AppCompatActivity

open class BaseActivity : AppCompatActivity() {

    protected var basePresenter: BasePresenter? = null

    override fun onStop() {
        basePresenter?.onStop()
        super.onStop()
    }
}