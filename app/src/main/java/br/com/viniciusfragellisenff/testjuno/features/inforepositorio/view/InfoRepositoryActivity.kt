package br.com.viniciusfragellisenff.testjuno.features.inforepositorio.view

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.MenuItem
import br.com.viniciusfragellisenff.testjuno.R
import br.com.viniciusfragellisenff.testjuno.bases.BaseActivity
import br.com.viniciusfragellisenff.testjuno.features.listabusca.view.adapter.ListaInfoRepoAdapter
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import kotlinx.android.synthetic.main.activity_info_repository.*

class InfoRepositoryActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_info_repository)
        rvInfoRepo.layoutManager = LinearLayoutManager(this)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Informações"
        supportActionBar?.setHomeButtonEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        rvInfoRepo.adapter = ListaInfoRepoAdapter(this,intent.getSerializableExtra("repo") as RepositorioEntity)
    }


    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if(item?.itemId == android.R.id.home){
            finish()
        }
        return super.onOptionsItemSelected(item)
    }
}
