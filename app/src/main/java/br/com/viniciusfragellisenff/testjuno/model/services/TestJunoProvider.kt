package br.com.viniciusfragellisenff.testjuno.model.services

import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

open class TestJunoProvider {
    companion object {
        private var service: TestJunoService? = null
        fun getTestJunoService(): TestJunoService {
            return service ?: createBuild()
        }

        private fun createBuild(): TestJunoService {
            var retrofit = Retrofit.Builder()
                    .baseUrl("https://api.github.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build()
            val build = retrofit.create(TestJunoService::class.java)
            service = build
            return build
        }
    }
}