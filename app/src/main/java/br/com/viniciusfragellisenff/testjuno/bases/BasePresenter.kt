package br.com.viniciusfragellisenff.testjuno.bases

import io.reactivex.disposables.CompositeDisposable

open class BasePresenter() {

    protected var compositeDisposable: CompositeDisposable = CompositeDisposable()

    fun onStop() {
        compositeDisposable.clear()
    }

    fun stopAllBoucesAndsTasks(){
        compositeDisposable.clear()
    }

}