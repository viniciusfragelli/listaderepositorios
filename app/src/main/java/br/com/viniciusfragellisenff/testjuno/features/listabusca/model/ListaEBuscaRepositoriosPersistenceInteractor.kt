package br.com.viniciusfragellisenff.testjuno.features.listabusca.model

import android.content.Context
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import br.com.viniciusfragellisenff.testjuno.model.database.DAORepositorios
import br.com.viniciusfragellisenff.testjuno.model.database.DataBaseHelper
import br.com.viniciusfragellisenff.testjuno.model.database.ProviderRepositorios
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class ListaEBuscaRepositoriosPersistenceInteractor(context: Context,private var compositeDisposable: CompositeDisposable) {

    private var dataBaseHelper: DataBaseHelper
    private var disposable: Disposable? = null

    private var daoRepositorios: DAORepositorios

    init {
        dataBaseHelper = DataBaseHelper(context)
        dataBaseHelper.writableDatabase
        dataBaseHelper.readableDatabase
        daoRepositorios = DAORepositorios(ProviderRepositorios(dataBaseHelper))
    }

    fun processaBuscaEArmazenaResultados(lista: List<RepositorioEntity>){
        compositeDisposable.add(Single.fromCallable({
            for(item in lista){
                item.getValues();
                daoRepositorios.save(item)
            }
        }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribe({
            println("SALVO COM SUCESSO")
        },{ex ->
            ex.printStackTrace()
        }))
    }

    fun getBuscaRepoitorios(busca: String, onSuccess: ((List<RepositorioEntity>) -> Unit), onError: ((Throwable) -> Unit)){
        if(disposable != null)compositeDisposable.remove(disposable!!)
        disposable = Observable.fromCallable<List<RepositorioEntity>>{
            daoRepositorios.queryBySearch(busca)
        }.debounce(2000, TimeUnit.MICROSECONDS)
                .filter({ if(busca.isNullOrEmpty()) false else true })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    onSuccess(response)
                },{ ex ->
                    onError(ex)
                })
        compositeDisposable.add(disposable ?: return)
    }

    fun cancelarBusca(){
        compositeDisposable.remove(disposable ?: return)
    }
}