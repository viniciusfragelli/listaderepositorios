package br.com.viniciusfragellisenff.testjuno.features.listabusca.view.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import br.com.viniciusfragellisenff.testjuno.R
import br.com.viniciusfragellisenff.testjuno.extensions.toFormatBrazil
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import kotlinx.android.synthetic.main.repositorio_adapter.view.*
import kotlinx.android.synthetic.main.repositorio_item_adapter.view.*
import java.lang.reflect.Field

class ListaInfoRepoAdapter(private val context: Context, private var repo: RepositorioEntity) : RecyclerView.Adapter<InfoRepoViewHolder>() {

    private val lista: List<Field>

    init {
        lista = RepositorioEntity::class.java.declaredFields.toList()
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, position: Int): InfoRepoViewHolder = InfoRepoViewHolder(LayoutInflater.from(context).inflate(R.layout.repositorio_item_adapter, viewGroup, false))

    override fun getItemCount(): Int = lista.size

    override fun onBindViewHolder(view: InfoRepoViewHolder, position: Int) {
        val field = lista[position]
        with(view.itemView){
            tvCampo.text = field.name
            field.isAccessible = true
            tvValue.text = "${field.get(repo)}"
        }
    }


}

class InfoRepoViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

}
