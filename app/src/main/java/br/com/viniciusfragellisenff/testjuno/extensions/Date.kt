package br.com.viniciusfragellisenff.testjuno.extensions

import java.text.SimpleDateFormat
import java.util.*

fun Date?.toFormatBrazil(): String? = if(this == null) null else SimpleDateFormat("dd/MM/YYYY").format(this)