package br.com.viniciusfragellisenff.testjuno.features.listabusca.presenter

import android.content.Context
import br.com.viniciusfragellisenff.testjuno.bases.BasePresenter
import br.com.viniciusfragellisenff.testjuno.features.listabusca.model.ListaEBuscaRepositoriosPersistenceInteractor
import br.com.viniciusfragellisenff.testjuno.features.listabusca.model.ListaEBuscaRepositoriosServiceInteractor
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity

class ListaEBuscaRepositoriosPresenter(private val context: Context, private val viewCallback: ListaEBuscaRepositoriosViewCallback) : BasePresenter() {

    private val interactorService: ListaEBuscaRepositoriosServiceInteractor by lazy { ListaEBuscaRepositoriosServiceInteractor(compositeDisposable) }
    private val interactorPersistenceInteractor: ListaEBuscaRepositoriosPersistenceInteractor by lazy { ListaEBuscaRepositoriosPersistenceInteractor(context, compositeDisposable) }
    var isOfflineMode: Boolean = false

    interface ListaEBuscaRepositoriosViewCallback{
        fun onBuscaRepositorioError()
        fun onSucessoBuscaRepositorio(lista: List<RepositorioEntity>)
    }

    fun taskBuscaRepositorio(busca: String){
        if (isOfflineMode){
            interactorPersistenceInteractor.getBuscaRepoitorios(busca,{
                viewCallback.onSucessoBuscaRepositorio(it)
            },{
                it.printStackTrace()
                viewCallback.onBuscaRepositorioError()
            })
        }else {
            interactorService.getBuscaRepositorios(busca, {
                interactorPersistenceInteractor.processaBuscaEArmazenaResultados(it.items)
                viewCallback.onSucessoBuscaRepositorio(it.items)
            }, {
                it.printStackTrace()
                viewCallback.onBuscaRepositorioError()
            })
        }
    }

    fun cancelarBusca(){
        if(isOfflineMode){
            interactorPersistenceInteractor.cancelarBusca()
        }else{
            interactorService.cancelarBusca()
        }
    }
}