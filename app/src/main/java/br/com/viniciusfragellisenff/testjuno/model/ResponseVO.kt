package br.com.viniciusfragellisenff.testjuno.model

import com.j256.ormlite.field.DatabaseField
import java.util.*

class ResponseVO {
    var total_count: Int = 0
    var incomplete_results: Boolean = false
    var items = mutableListOf<RepositorioEntity>()
}