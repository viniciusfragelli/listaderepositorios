package br.com.viniciusfragellisenff.testjuno.model

import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import java.io.Serializable
import java.util.*

@DatabaseTable(tableName = "RepositorioEntity")
class RepositorioEntity : Serializable{

    @DatabaseField(id = true)
    var id: Int = 0
    @DatabaseField
    var node_id: String? = null
    @DatabaseField
    var name: String? = null
    @DatabaseField
    var full_name: String? = null
    @DatabaseField
    var private: Boolean = false

    var owner: OwnerVO = OwnerVO()

    @DatabaseField
    var html_url: String? = null
    @DatabaseField
    var description: String? = null
    @DatabaseField
    var fork: Boolean = false
    @DatabaseField
    var url: String? = null
    @DatabaseField
    var forks_url: String? = null
    @DatabaseField
    var keys_url: String? = null
    @DatabaseField
    var collaborators_url: String? = null
    @DatabaseField
    var teams_url: String? = null
    @DatabaseField
    var hooks_url: String? = null
    @DatabaseField
    var issue_events_url: String? = null
    @DatabaseField
    var events_url: String? = null
    @DatabaseField
    var assignees_url: String? = null
    @DatabaseField
    var branches_url: String? = null
    @DatabaseField
    var tags_url: String? = null
    @DatabaseField
    var blobs_url: String? = null
    @DatabaseField
    var git_tags_url: String? = null
    @DatabaseField
    var git_refs_url: String? = null
    @DatabaseField
    var trees_url: String? = null
    @DatabaseField
    var statuses_url: String? = null
    @DatabaseField
    var languages_url: String? = null
    @DatabaseField
    var stargazers_url: String? = null
    @DatabaseField
    var contributors_url: String? = null
    @DatabaseField
    var subscribers_url: String? = null
    @DatabaseField
    var subscription_url: String? = null
    @DatabaseField
    var commits_url: String? = null
    @DatabaseField
    var git_commits_url: String? = null
    @DatabaseField
    var comments_url: String? = null
    @DatabaseField
    var issue_comment_url: String? = null
    @DatabaseField
    var contents_url: String? = null
    @DatabaseField
    var compare_url: String? = null
    @DatabaseField
    var merges_url: String? = null
    @DatabaseField
    var archive_url: String? = null
    @DatabaseField
    var downloads_url: String? = null
    @DatabaseField
    var issues_url: String? = null
    @DatabaseField
    var pulls_url: String? = null
    @DatabaseField
    var milestones_url: String? = null
    @DatabaseField
    var notifications_url: String? = null
    @DatabaseField
    var labels_url: String? = null
    @DatabaseField
    var releases_url: String? = null
    @DatabaseField
    var deployments_url: String? = null
    @DatabaseField
    var created_at: Date? = null
    @DatabaseField
    var updated_at: Date? = null
    @DatabaseField
    var pushed_at: Date? = null
    @DatabaseField
    var git_url: String? = null
    @DatabaseField
    var ssh_url: String? = null
    @DatabaseField
    var clone_url: String? = null
    @DatabaseField
    var svn_url: String? = null
    @DatabaseField
    var homepage: String? = null
    @DatabaseField
    var size: Int = 0
    @DatabaseField
    var stargazers_count: Int = 0
    @DatabaseField
    var watchers_count: Int = 0
    @DatabaseField
    var language: String? = null
    @DatabaseField
    var has_issues: Boolean = false
    @DatabaseField
    var has_projects: Boolean = false
    @DatabaseField
    var has_downloads: Boolean = false
    @DatabaseField
    var has_wiki: Boolean = false
    @DatabaseField
    var has_pages: Boolean = false
    @DatabaseField
    var forks_count: Int = 0
    @DatabaseField
    var mirror_url: String? = null
    @DatabaseField
    var archived: Boolean = false
    @DatabaseField
    var disabled: Boolean = false
    @DatabaseField
    var open_issues_count: Int = 0

    var license: LicenseVO = LicenseVO()

    @DatabaseField
    var forks: Int = 0
    @DatabaseField
    var open_issues: Int = 0
    @DatabaseField
    var watchers: Int = 0
    @DatabaseField
    var default_branch: String? = null
    @DatabaseField
    var score: Double = 0.0

    @DatabaseField
    var license_key: String? = null
    @DatabaseField
    var license_name: String? = null
    @DatabaseField
    var license_spdx_id: String? = null
    @DatabaseField
    var license_url: String? = null
    @DatabaseField
    var license_node_id: String? = null

    @DatabaseField
    var owner_login: String? = null
    @DatabaseField
    var owner_id: Int = 0
    @DatabaseField
    var owner_node_id: String? = null
    @DatabaseField
    var owner_avatar_url: String? = null
    @DatabaseField
    var owner_gravatar_id: String? = null
    @DatabaseField
    var owner_url: String? = null
    @DatabaseField
    var owner_html_url: String? = null
    @DatabaseField
    var owner_followers_url: String? = null
    @DatabaseField
    var owner_following_url: String? = null
    @DatabaseField
    var owner_gists_url: String? = null
    @DatabaseField
    var owner_starred_url: String? = null
    @DatabaseField
    var owner_subscriptions_url: String? = null
    @DatabaseField
    var owner_organizations_url: String? = null
    @DatabaseField
    var owner_repos_url: String? = null
    @DatabaseField
    var owner_events_url: String? = null
    @DatabaseField
    var owner_received_events_url: String? = null
    @DatabaseField
    var owner_type: String? = null
    @DatabaseField
    var owner_site_admin: Boolean = false

    inner class LicenseVO : Serializable{
        var key: String? = null
        var name: String? = null
        var spdx_id: String? = null
        var url: String? = null
        var node_id: String? = null
    }

    inner class OwnerVO : Serializable{
        var login: String? = null
        var id: Int = 0
        var node_id: String? = null
        var avatar_url: String? = null
        var gravatar_id: String? = null
        var url: String? = null
        var html_url: String? = null
        var followers_url: String? = null
        var following_url: String? = null
        var gists_url: String? = null
        var starred_url: String? = null
        var subscriptions_url: String? = null
        var organizations_url: String? = null
        var repos_url: String? = null
        var events_url: String? = null
        var received_events_url: String? = null
        var type: String? = null
        var site_admin: Boolean = false
    }

    fun getValues() {
        if (owner != null) {
            owner_login = owner.login;
            owner_id = owner.id
            owner_node_id = owner.node_id
            owner_avatar_url = owner.avatar_url
            owner_gravatar_id = owner.gravatar_id
            owner_url = owner.url
            owner_html_url = owner.html_url
            owner_followers_url = owner.followers_url
            owner_following_url = owner.following_url
            owner_gists_url = owner.gists_url
            owner_starred_url = owner.starred_url
            owner_subscriptions_url = owner.subscriptions_url
            owner_organizations_url = owner.organizations_url
            owner_repos_url = owner.repos_url
            owner_events_url = owner.received_events_url
            owner_received_events_url = owner.received_events_url
            owner_type = owner.type
            owner_site_admin = owner.site_admin
        }
        if (license != null) {
            license_key = license.key
            license_name = license.name
            license_spdx_id = license.spdx_id
            license_url = license.url
            license_node_id = license.node_id
        }
    }
}