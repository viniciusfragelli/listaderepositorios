package br.com.viniciusfragellisenff.testjuno.model.services

import br.com.viniciusfragellisenff.testjuno.model.ResponseVO
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

open interface TestJunoService {
    @GET("search/repositories")
    fun buscaRepositorio(@Query("q") q: String): Observable<ResponseVO>
}
