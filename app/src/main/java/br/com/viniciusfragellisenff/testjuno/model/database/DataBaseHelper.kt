package br.com.viniciusfragellisenff.testjuno.model.database

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils

class DataBaseHelper(context: Context) : OrmLiteSqliteOpenHelper(context,"repositorios.db",null,1) {
    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        println("Banco de dados criado!")
        TableUtils.createTable(connectionSource,RepositorioEntity::class.java)
    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        println("Banco atualizado com suscesso!")
    }

}