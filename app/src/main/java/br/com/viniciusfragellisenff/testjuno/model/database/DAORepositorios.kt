package br.com.viniciusfragellisenff.testjuno.model.database

import br.com.viniciusfragellisenff.testjuno.model.RepositorioEntity
import com.j256.ormlite.dao.RuntimeExceptionDao
import java.sql.SQLException
import javax.inject.Inject

class DAORepositorios(private var provider: ProviderRepositorios) {

    private var entityDao: RuntimeExceptionDao<RepositorioEntity, String>? = null

    init {
        entityDao = provider.get()
    }

    fun save(repo: RepositorioEntity) : Int? {
        if(queryByID(repo.id)?.size == 0) {
            return entityDao?.create(repo)
        }
        return null
    }

    @Throws(SQLException::class)
    fun queryByID(id: Int): List<RepositorioEntity>? {
        val builder = entityDao?.queryBuilder()
        builder?.where()?.eq("id", id)
        return entityDao?.query(builder?.prepare())
    }

    @Throws(SQLException::class)
    fun queryBySearch(busca: String): List<RepositorioEntity>? {
        val builder = entityDao?.queryBuilder()
        builder?.where()?.like("name", "%"+busca+"%")?.or()?.like("full_name","%"+busca+"%")?.or()?.like("owner_login", "%"+busca+"%")?.or()?.like("html_url", "%"+busca+"%")?.or()?.like("url", "%"+busca+"%")?.or()?.like("description", "%"+busca+"%")
        return entityDao?.query(builder?.prepare())
    }


    fun saveList(lista: List<RepositorioEntity>){
        for(repo in lista) {
            save(repo)
        }
    }

}